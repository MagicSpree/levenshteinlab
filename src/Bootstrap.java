public class Bootstrap {
    public static void main(String[] args) {
        Levenshtein levenshtein = new Levenshtein();
        int result = levenshtein.getDistance("POLYNOMIAL", "EXPONENTIAL");
        System.out.println("The Levenshtein distance is " + result);
    }
}
