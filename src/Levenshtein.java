import java.util.Arrays;
import java.util.Locale;

public class Levenshtein {

    public int getDistance(String firstStr, String secondStr) {
        if (firstStr == null || secondStr == null) return -1;
        if (firstStr.isEmpty()) return secondStr.length();
        if (secondStr.isEmpty()) return firstStr.length();

        String lowerFirstStr = toLowerRegister(firstStr);
        String lowerSecondStr = toLowerRegister(secondStr);

        int firstStrLength = lowerFirstStr.length();
        int secondStrLength = lowerSecondStr.length();

        int[][] matrix = new int[secondStrLength + 1][firstStrLength + 1];

        for (int i = 1; i <= secondStrLength; i++) {
            matrix[i][0] = i;
        }
        for (int j = 1; j <= firstStrLength; j++) {
            matrix[0][j] = j;
        }

        for (int i = 1; i <= secondStrLength; i++) {
            for (int j = 1; j <= firstStrLength; j++) {
                if (lowerSecondStr.charAt(i - 1) == lowerFirstStr.charAt(j - 1)) {
                    matrix[i][j] = matrix[i - 1][j - 1];
                } else {
                    matrix[i][j] = min(matrix[i - 1][j - 1] + 1,
                            matrix[i][j - 1] + 1,
                            matrix[i - 1][j] + 1);
                }
            }
        }
        return matrix[secondStrLength][firstStrLength];
    }

    private String toLowerRegister(String str) {
        return str.trim().toLowerCase(Locale.ROOT);
    }

    private int min(int... numbers) {
        return Arrays.stream(numbers)
                .min()
                .orElse(0);
    }

}
